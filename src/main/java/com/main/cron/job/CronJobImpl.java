package com.main.cron.job;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;

import com.main.cron.util.SolrUrls;
import com.main.solr.crud.SolrCrud;
import com.main.solr.crud.SolrCrudImpl;


public class CronJobImpl implements CronJob , SolrUrls{

	@Autowired
	SolrCrud solrCrud;
	
	
	@Override
	public String readRattingAnalytic() {
		
		solrCrud=new SolrCrudImpl();
		//Read all records
		QueryResponse response=solrCrud.serachDocument(SolrUrls.solrAnalyticUrl, "*:*");
		
		final SolrDocumentList documents = response.getResults();
		//If any of record there for analytic
		if(documents.getNumFound()>0)
		{	
			//Start itterate document
			documents.forEach((K)-> {
				//Start looking up If documnt is already being calculate
				QueryResponse rattingAnalyticResponse=solrCrud.serachDocument(SolrUrls.commentRattingAnalytic, "contentId:"+(String)K.get("contentId"));
				System.out.println("rattingAnalyticResponse         " + rattingAnalyticResponse);
				if(rattingAnalyticResponse.getResults().getNumFound()>0) {
					
					//If document is already ratted or callculated for Analytic
					
					SolrDocumentList alreadyDocument=rattingAnalyticResponse.getResults();
					
					alreadyDocument.forEach((already)-> {
						solrCrud.addDocument(SolrUrls.commentRattingAnalytic, createAlreadyDocument((String)K.get("contentId"),(String)K.get("ratting"),already));
						deleteDocById((String)K.get("reviewId"));
					});					
					
				}else {
					// Added Artting analytic for first time content.
					solrCrud.addDocument(SolrUrls.commentRattingAnalytic, createFirstDocument((String)K.get("contentId"),(String)K.get("ratting")));
					deleteDocById((String)K.get("reviewId"));
				}		
			});		
		}
		return "sucess";
	}

	@Override
	public String deleteDocById(String reviewId) {		
		solrCrud=new SolrCrudImpl();
		String query="reviewId"+":"+reviewId;		
		UpdateResponse response=solrCrud.deleteByQuery(SolrUrls.solrAnalyticUrl, query);		
		return "sucess";
	}
	
	public SolrInputDocument createFirstDocument(String contentId, String ratting) {
		SolrInputDocument document = new SolrInputDocument();	
		
		document.addField("contentId", contentId);		
		document.addField("rattingCount", "1");
		
		switch(ratting) {
		case "1": 
			document.addField("ratting1", 1);
			document.addField("ratting2", 0);
			document.addField("ratting3", 0);
			document.addField("ratting4", 0);
			document.addField("ratting5", 0);	
			document.addField("averageRatting", 1);
			break;
			
		case "2": 
			document.addField("ratting1", 0);
			document.addField("ratting2", 1);
			document.addField("ratting3", 0);
			document.addField("ratting4", 0);
			document.addField("ratting5", 0);		
			document.addField("averageRatting", 2);
			break;
		
		case "3": 
			document.addField("ratting1", 0);
			document.addField("ratting2", 0);
			document.addField("ratting3", 1);
			document.addField("ratting4", 0);
			document.addField("ratting5", 0);	
			document.addField("averageRatting", 3);
			break;
		
		case "4": 
			document.addField("ratting1", 0);
			document.addField("ratting2", 0);
			document.addField("ratting3", 0);
			document.addField("ratting4", 1);
			document.addField("ratting5", 0);	
			document.addField("averageRatting", 4);
			break;
		
		case "5": 
			document.addField("ratting1", 0);
			document.addField("ratting2", 0);
			document.addField("ratting3", 0);
			document.addField("ratting4", 0);
			document.addField("ratting5", 1);	
			document.addField("averageRatting", 5);	
			break;		
		}
		return document;
	}
	
	public SolrInputDocument createAlreadyDocument(String contentId, String ratting, SolrDocument solrdoc) {
		SolrInputDocument document = new SolrInputDocument();		
		document.addField("contentId", contentId);		
		document.addField("rattingCount", (int)solrdoc.get("rattingCount")+1);
		int starRateOne = (int)solrdoc.get("ratting1");
		int starRateTwo = (int)solrdoc.get("ratting2");
		int starRateThree = (int)solrdoc.get("ratting3");
		int starRateFour = (int)solrdoc.get("ratting4");
		int starRateFive = (int)solrdoc.get("ratting5");
		
		switch(ratting) {
		case "1": 
			starRateOne = starRateOne + 1;			
			break;			
		case "2": 
			starRateTwo = starRateTwo + 1;
			break;		
		case "3": 
			starRateThree = starRateThree + 1;
			break;		
		case "4": 
			starRateFour = starRateFour + 1;
			break;		
		case "5": 
			starRateFive = starRateFive + 1;
			break;		
		}
		
		double countRatings=((1*starRateOne)+(2*starRateTwo)+(3*starRateThree)+(4*starRateFour)+(5*starRateFive));
		double totalRatings = (starRateOne + starRateTwo + starRateThree + starRateFour + starRateFive);
		
		double averageRatting= Math.round((countRatings/totalRatings) * 10) / 10.0; 		/*rounding average to one decimal place*/
		System.out.println("averageRatting === " + averageRatting + " === "  + countRatings  + " === "  + totalRatings);
		
		document.addField("ratting1", starRateOne);
		document.addField("ratting2", starRateTwo);
		document.addField("ratting3", starRateThree);
		document.addField("ratting4", starRateFour);
		document.addField("ratting5", starRateFive);
		document.addField("averageRatting", averageRatting);
		return document;
	}
	
	public SolrInputDocument createAlreadyDocument2(String contentId, String ratting, SolrDocument solrdoc) {
		SolrInputDocument document = new SolrInputDocument();		
		document.addField("contentId", contentId);		
		document.addField("rattingCount", (int)solrdoc.get("rattingCount")+1);
		
	//	int averageRatting=((1*(int)solrdoc.get("ratting1"))+(2*(int)solrdoc.get("ratting2"))+(3*(int)solrdoc.get("ratting3"))+(4*(int)solrdoc.get("ratting4"))+(5*(int)solrdoc.get("ratting5")))/(int)solrdoc.get("ratting1")+(int)solrdoc.get("ratting2")+(int)solrdoc.get("ratting5")+(int)solrdoc.get("ratting4")+(int)solrdoc.get("ratting5");
		
		int rattingCount=(int)solrdoc.get("ratting1")+(int)solrdoc.get("ratting2")+(int)solrdoc.get("ratting3")+(int)solrdoc.get("ratting4")+(int)solrdoc.get("ratting5");
		int averageRatting=rattingCount/5;
		
		
		switch(ratting) {
		case "1": 
			document.addField("ratting1", (int)solrdoc.get("ratting1")+1);
			document.addField("ratting2", (int)solrdoc.get("ratting2"));
			document.addField("ratting3", (int)solrdoc.get("ratting3"));
			document.addField("ratting4", (int)solrdoc.get("ratting4"));
			document.addField("ratting5", (int)solrdoc.get("ratting5"));
			document.addField("averageRatting", averageRatting);		
			break;
			
		case "2": 
			document.addField("ratting1", (int)solrdoc.get("ratting1"));
			document.addField("ratting2", (int)solrdoc.get("ratting2")+1);
			document.addField("ratting3", (int)solrdoc.get("ratting3"));
			document.addField("ratting4", (int)solrdoc.get("ratting4"));
			document.addField("ratting5", (int)solrdoc.get("ratting5"));
			document.addField("averageRatting", averageRatting);	
			break;
		
		case "3": 
			document.addField("ratting1", (int)solrdoc.get("ratting1"));
			document.addField("ratting2", (int)solrdoc.get("ratting2"));
			document.addField("ratting3", (int)solrdoc.get("ratting3")+1);
			document.addField("ratting4", (int)solrdoc.get("ratting4"));
			document.addField("ratting5", (int)solrdoc.get("ratting5"));
			document.addField("averageRatting", averageRatting);
			break;
		
		case "4": 
			document.addField("ratting1", (int)solrdoc.get("ratting1"));
			document.addField("ratting2", (int)solrdoc.get("ratting2"));
			document.addField("ratting3", (int)solrdoc.get("ratting3"));
			document.addField("ratting4", (int)solrdoc.get("ratting4")+1);
			document.addField("ratting5", (int)solrdoc.get("ratting5"));			
			document.addField("averageRatting", averageRatting);
			break;
		
		case "5": 
			document.addField("ratting1", (int)solrdoc.get("ratting1"));
			document.addField("ratting2", (int)solrdoc.get("ratting2"));
			document.addField("ratting3", (int)solrdoc.get("ratting3"));
			document.addField("ratting4", (int)solrdoc.get("ratting4"));
			document.addField("ratting5", (int)solrdoc.get("ratting5")+1);			
			document.addField("averageRatting", averageRatting);
			break;		
		}
		return document;
	}

	@Override
	public String deletebyQuery(String solrUrl, String query) {
		solrCrud=new SolrCrudImpl();
		
		UpdateResponse response =solrCrud.deleteByQuery(solrUrl, query);
		if (response!=null)		
		return "sucess";
		else 
			return "false";
	}
	
}
