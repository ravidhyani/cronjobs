package com.main.solr.crud;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;

public interface SolrCrud {

	public QueryResponse serachDocument(String solrUrl, String query);
	
	public UpdateResponse deleteByQuery(String solrUrl, String query);
	
	public String addDocument(String solrUrl,SolrInputDocument document);
	
}