package com.main.cron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.main.cron.job.CronJob;
import com.main.cron.job.CronJobImpl;
import com.main.cron.util.SolrUrls;

@SpringBootApplication
public class CronJobsApplication {

	@Autowired
	static CronJob cronJob;

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(CronJobsApplication.class, args);

		/*cronJob = new CronJobImpl();
		while (true) {
			// System.out.println("---------------"+cronJob.readRattingAnalytic());
			// System.out.println("---------------"+cronJob.deletebyQuery(SolrUrls.commentRattingAnalytic,
			// "*:*"));
			// System.out.println("---------------"+cronJob.deletebyQuery("http://localhost:8983/solr/gettingstarted",
			// "*:*"));

			// Thread.sleep(1000);
		}*/
	}
}
