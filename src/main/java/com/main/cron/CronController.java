package com.main.cron;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.main.cron.job.CronJob;
import com.main.cron.job.CronJobImpl;

@RestController
public class CronController {
	@Autowired
	static
	CronJob cronJob;
	
	@RequestMapping("/ratingAnalysis") 
	public String ratingAnalysis() {
		cronJob=new CronJobImpl();
		System.out.println("--------------->>> in cron control ");
		System.out.println("--------------->>> "+cronJob.readRattingAnalytic());
		System.out.println("--------------->>> OUT cron control ");
		//	System.out.println("---------------"+cronJob.deletebyQuery(SolrUrls.commentRattingAnalytic, "*:*"));
	    //	System.out.println("---------------"+cronJob.deletebyQuery("http://localhost:8983/solr/gettingstarted", "*:*"));
		return "OK";
	}
}
