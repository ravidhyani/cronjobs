package com.main.cron.util;

import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;

public class CronJobUtility {

	public static HttpSolrClient getSolrClient(String solrUrl) {
		HttpSolrClient solr = new HttpSolrClient.Builder(solrUrl).build();
		solr.setParser(new XMLResponseParser());
		return solr;
	}
}