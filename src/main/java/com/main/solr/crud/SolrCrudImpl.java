package com.main.solr.crud;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.springframework.stereotype.Component;

import com.main.cron.util.CronJobUtility;

@Component
public class SolrCrudImpl implements SolrCrud {

	@Override
	public QueryResponse serachDocument(String solrUrl, String query) {
		
		QueryResponse response=null;
		HttpSolrClient solr=CronJobUtility.getSolrClient(solrUrl);
		solr.setParser(new XMLResponseParser());
		
		final Map<String, String> queryParamMap = new HashMap<String, String>();
		queryParamMap.put("q", query);
		// queryParamMap.put("fl", "id, name");
		MapSolrParams queryParams = new MapSolrParams(queryParamMap);
		
		try {
			response=solr.query(queryParams);
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public UpdateResponse deleteByQuery(String solrUrl, String query) {
		HttpSolrClient solr=CronJobUtility.getSolrClient(solrUrl);
		solr.setParser(new XMLResponseParser());
		UpdateResponse response = null;
		try {
			response = solr.deleteByQuery(query);
			solr.commit();
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return response;
	}

	@Override
	public String addDocument(String solrUrl, SolrInputDocument document) {
		HttpSolrClient solr = new HttpSolrClient.Builder(solrUrl).build();
		solr.setParser(new XMLResponseParser());
	
		try {
			solr.add(document);
			solr.commit();
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return "sucess";
	}
}